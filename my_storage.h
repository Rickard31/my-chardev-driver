#ifndef MY_STORAGE_H
#define MY_STORAGE_H

#include <linux/radix-tree.h>

#include "my_driver_data.h"

RADIX_TREE(my_root, GFP_ATOMIC);

static struct my_driver_data *get_my_driver_data(__u32 minor_num) {
    struct my_driver_data *data;
    
    data = (struct my_driver_data *) radix_tree_lookup(&my_root, minor_num);
    if(data)
        return data;

    data = init_my_driver_data(minor_num);
    radix_tree_insert(&my_root, minor_num, data);
    return data;
}


#endif // MY_STORAGE_H