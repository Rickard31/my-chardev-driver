#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <asm/uaccess.h>

#include "my_driver_data.h"
#include "my_storage.h"

#define SUCCESS 0
#define DEVICE_NAME "my_device"

static int major_num;

static int device_open(struct inode *arg_inode, struct file *arg_file)
{
	__u32 minor_num = MINOR(arg_inode->i_rdev);
	struct my_driver_data *device = get_my_driver_data(minor_num);

	printk(KERN_INFO "device_open(%p)\n", arg_file);
	if (device->is_device_open)
		return -EBUSY;

	(device->is_device_open)++;
	device->message_ptr = device->message;
	try_module_get(THIS_MODULE);
	return SUCCESS;
}

static int device_release(struct inode *arg_inode, struct file *file)
{
	__u32 minor_num = MINOR(arg_inode->i_rdev);
	struct my_driver_data *device = get_my_driver_data(minor_num);

	printk(KERN_INFO "device_release(%p,%p)\n", arg_inode, file);

	(device->is_device_open)--;

	module_put(THIS_MODULE);
	return SUCCESS;
}

static ssize_t device_read(struct file *file,	
			   char __user * buffer,	
			   size_t length,
			   loff_t * offset)
{
	__u32 minor_num = MINOR(file->f_inode->i_rdev);
	struct my_driver_data *device = get_my_driver_data(minor_num);
	ssize_t bytes_read = 0;

	printk(KERN_INFO "device_read(%p,%p,%ld)\n", file, buffer, length);

	if (*(device->message_ptr) == 0)
		return 0;

	while (length && *(device->message_ptr)) {
		put_user(*((device->message_ptr)++), buffer++);
		length--;
		bytes_read++;
	}

	printk(KERN_INFO "Read %ld bytes, %ld left\n", bytes_read, length);
	return bytes_read;
}

static ssize_t
device_write(struct file *file,
	     const char __user * buffer, size_t length, loff_t * offset)
{
	__u32 minor_num = MINOR(file->f_inode->i_rdev);
	struct my_driver_data *device = get_my_driver_data(minor_num);

	int i = 0;

	printk(KERN_INFO "device_write(%p,%s,%ld)", file, buffer, length);

	memset(device->message, 0, BUFFER_LEN*sizeof(*(device->message)));

	for (i = 0; i < length && i < BUFFER_LEN; i++)
		get_user((device->message)[i], buffer + i);

	device->message_ptr = device->message;

	return i;
}

struct file_operations fops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release,
};

int __init init_module(void)
{
	major_num = register_chrdev(0, DEVICE_NAME, &fops);

	if (major_num < 0) {
		printk("Registering the character device driver failed with %d\n", major_num);
		return major_num;
	}

	printk(KERN_WARNING "My_device was assigned major number %d. To talk to\n", major_num);
	printk(KERN_WARNING "the driver, create a dev file with\n");
	printk(KERN_WARNING "'sudo mknod /dev/<device-name> c %d <minor-num>'.\n", major_num);
	printk(KERN_WARNING "Remove device files and module when done.\n");

	return SUCCESS;
}

void __exit cleanup_module(void)
{
	unregister_chrdev(major_num, DEVICE_NAME);
	printk(KERN_WARNING "Removed my_driver from kernel.\n");
}

MODULE_AUTHOR("Vitalii Marchenko <vitalka.marchenko@gmail.com");
MODULE_LICENSE("GPL");