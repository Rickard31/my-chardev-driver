#ifndef MY_DRIVER_DATA_H
#define MY_DRIVER_DATA_H

#include <linux/slab.h>

#define BUFFER_LEN 255
#define INITIAL_MESSAGE "Initial content of my device\n"

struct my_driver_data {
    __u32 minor_num;
    int is_device_open;
    char message[BUFFER_LEN];
    char *message_ptr;
};

static struct my_driver_data *init_my_driver_data(__u32 minor_num) {
    struct my_driver_data *res = kmalloc(sizeof(struct my_driver_data), GFP_KERNEL);
    if(res) {
        res->minor_num = minor_num;
        res->is_device_open = 0;
        
        memset(res->message, 0, BUFFER_LEN*sizeof(*(res->message)));
        snprintf(res->message, BUFFER_LEN, INITIAL_MESSAGE);
        
        res->message_ptr = res->message;
    }
    return res;
}

#endif // MY_DRIVER_DATA_H